// these are only partial typings, you may need to add more as needed
// refer to xidjs repo - https://bitbucket.org/affinipay/xidjs/src/master/src/

declare module "xidjs" {
  export class XID {
    public rawIdBitLength: number;
    public rawIdBytes: number;
  }

  export function xidToUuidBase64(xid: XID): string;

  export function uuidBase64ToXid(
    uuid: string,
    xidFactory: XIDFactory,
    prefix?: string
  ): XID;

  export class XIDFactory {
    private constructor(); // note this is actually public, but we can add typings later
    public static defaultInstance: XIDFactory;
    public parse(xid: string, defaultPrefix?: string): XID;
    public random(prefix?: string): XID;
  }
}
