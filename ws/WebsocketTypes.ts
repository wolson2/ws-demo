export type WebsocketEventHandler = (data: any) => void;

export interface WebsocketListener {
  eventName: string;
  handler: WebsocketEventHandler;
}

export interface WebsocketEventData {
  sessionXid?: string;
}

export interface WebsocketEvent<T extends WebsocketEventData = any> {
  eventName: string;
  data: T;
}

export const wsEventNames = {
  PING: "ping",
  PONG: "pong",
  SESSION_CREATED: "session:created"
};
