import WebsocketServer from "./WebsocketServer";

export default class WssService {
  protected server: WebsocketServer;

  constructor(wsServer: WebsocketServer) {
    this.server = wsServer;
  }

  public start() {
    this.server.listen();
  }
}
