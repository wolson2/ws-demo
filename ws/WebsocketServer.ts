import Websocket, { ServerOptions, Server } from "ws";
import { XIDFactory } from "xidjs";
import {
  WebsocketEvent,
  WebsocketListener,
  wsEventNames,
} from "./WebsocketTypes";
import config from "./config";

const defaultOptions: ServerOptions = {
  port: parseInt(config.port, 10),
};

export default class WebsocketServer {
  private listeners: Map<string, WebsocketListener[]> = new Map();
  private server?: Server;
  private options: ServerOptions;
  private connections: Map<string, Websocket> = new Map();

  constructor(options: ServerOptions = defaultOptions) {
    this.on = this.on.bind(this);
    this.send = this.send.bind(this);
    this.listen = this.listen.bind(this);
    this.handleMessage = this.handleMessage.bind(this);
    this.setConnectionHandlers = this.setConnectionHandlers.bind(this);
    this.options = options;
  }

  public on(eventName: string, handler: any) {
    const listener: WebsocketListener = { eventName, handler };
    const current: WebsocketListener[] = this.listeners.has(eventName)
      ? this.listeners.get(eventName)!
      : [];
    current?.push(listener);
    this.listeners.set(eventName, current);
  }

  public send(eventName: string, data: any) {
    if (!this.server?.clients && !this.server?.clients.size) {
      console.warn("Unable to send message. No clients connected.");
      return;
    }
    try {
      const wsEvent: WebsocketEvent = { eventName, data };
      const payload: string = JSON.stringify(wsEvent);
      this.server.clients.forEach((connection) => connection.send(payload));
    } catch (e) {
      console.error("Error sending message", e);
    }
  }

  public sendWithSession(sessionXid: string, eventName: string, data: any) {
    if (!this.connections.has(sessionXid)) {
      console.warn(
        `Can't send message. No client found with sessionXid: ${sessionXid}.`
      );
      return;
    }

    try {
      const connection: Websocket = this.connections.get(sessionXid)!;
      const wsEvent: WebsocketEvent = { eventName, data };
      const payload: string = JSON.stringify(wsEvent);
      connection.send(payload);
    } catch (e) {
      console.error("Error sending message", e);
    }
  }

  public listen() {
    this.server = new Websocket.Server(this.options);
    this.server.on("connection", this.setConnectionHandlers);
    console.log(`WebsocketServer listening on port: ${this.options.port}`);
  }

  private setConnectionHandlers(connection: Websocket) {
    // create new session id for connection
    const sessionEvent: WebsocketEvent = {
      eventName: wsEventNames.SESSION_CREATED,
      data: {
        sessionXid: XIDFactory.defaultInstance.random("wss").toString(),
        lastActivity: new Date(),
      },
    };
    connection.send(JSON.stringify(sessionEvent));
    this.connections.set(sessionEvent.data.sessionXid, connection);
    connection.on("message", (message: any) =>
      this.handleMessage(sessionEvent.data.sessionXid, message)
    );
  }

  private handleMessage(sessionXid: string, incomingData: any) {
    try {
      console.log("server msg recieved: " + incomingData);
      const wsEvent: WebsocketEvent = JSON.parse(incomingData);
      wsEvent.data = wsEvent.data || {};
      if (typeof wsEvent.data === "object") {
        wsEvent.data.sessionXid = sessionXid;
      }
      const listeners: WebsocketListener[] = this.listeners.has(
        wsEvent.eventName
      )
        ? this.listeners.get(wsEvent.eventName)!
        : [];
      for (const listener of listeners) {
        listener.handler(wsEvent.data);
      }
    } catch (e) {
      console.error("error parsing incoming data: ", e);
    }
  }
}
