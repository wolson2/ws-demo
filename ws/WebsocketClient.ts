import Websocket from 'ws';
import { WebsocketEvent, WebsocketEventHandler } from './WebsocketTypes';

export default class WebSocketClient {
  private connection?: Websocket;
  private handlers: Map<string, WebsocketEventHandler[]> = new Map();
  private serverUrl: string;

  constructor(url: string) {
    this.serverUrl = url;
    this.on = this.on.bind(this);
    this.send = this.send.bind(this);
    this.connect = this.connect.bind(this);
    this.handleServerMessage = this.handleServerMessage.bind(this);
    this.close = this.close.bind(this);
  }

  public async connect(): Promise<boolean> {
    return await new Promise((res: any, rej: any) => {
      try {
        this.connection = new Websocket(this.serverUrl);
        this.on = this.on.bind(this);
        this.send = this.send.bind(this);
        this.connection.on('open', () => res(true));
        this.connection.on('close', () => rej(false));
        this.connection.on("message", this.handleServerMessage.bind(this));
      }
      catch(e) {
        console.error(e);
        rej(false);
      }
    });
  }
​
  // register a handler for an event
  public on(eventName: string, handler: WebsocketEventHandler) {
    const handlers: WebsocketEventHandler[] = this.handlers.has(eventName)
      ? this.handlers.get(eventName)!
      : [];
    handlers.push(handler);
    this.handlers.set(eventName, handlers);
  }

  // remove a handler from the queued callbacks
  public rm(eventName: string, handler: WebsocketEventHandler) {
    let handlers: WebsocketEventHandler[] = this.handlers.has(eventName)
      ? this.handlers.get(eventName)!
      : [];
    handlers.filter(h => h === handler);
    this.handlers.set(eventName, handlers);
  }

  public close() {
    this.connection?.close();
  }
​
  // send an event over websockets
  public send(eventName: string, data: any = null) {
    if (!this.connection) {
      console.warn('Unable to send message. Not connected.');
    }
    const event: WebsocketEvent = { eventName, data };
    const payload: string = JSON.stringify(event);
    this.connection!.send(payload);
  }
​
  private handleServerMessage(serverMessage: string) {
    const event: WebsocketEvent = JSON.parse(serverMessage);
    if (!this.handlers.has(event.eventName)) {
      return;
    }
    const handlers: WebsocketEventHandler[] = this.handlers.get(event.eventName)!;
    for (const callback of handlers) {
      callback(event.data);
    }
  }
}