import WssService from "./WssService";

export default abstract class WssListener<T extends WssService> {
  protected service: T;
  constructor(wssService: T) {
    this.service = wssService;
    this.attach();
  }
  abstract attach(): void;
}
