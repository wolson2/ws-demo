## Simple Websocket Server/Client Demo

Demo that listens for / sends messages over Websockets.


### Server

```
# get dependencies
npm install

# build and run the websocket server
npm run server

# use custom port
WSPORT=3000 npm run server

```

### Client

```
# get dependencies
npm install

# build and run the websocket client
npm run client

# run the client without building
npm run send

# send custom data
npm run send -- someCustomChannel '{ "custom": [ "abc123" ] }'

# use custom port
WSPORT=3000 npm run send -- channelName 'hello'
```

> Note: If `npm install` fails, check jfrog config in `~/.npmrc` and perform an `npm login`, then try again.