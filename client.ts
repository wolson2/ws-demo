import getInputOrDefault from "./getInputOrDefault";
import WebsocketClient from "./ws/WebsocketClient";
import config from "./ws/config";
import { wsEventNames } from "./ws/WebsocketTypes";

interface Pong {
  timestamp: Date;
}

interface Session {
  sessionXid: string;
  lastActivity?: Date;
}
let sessionXid: string = "";
const onPong = (resp: Pong) => {
  console.log(
    `Pong (${sessionXid}) timestamp: ${new Date(resp.timestamp).toLocaleString()}`
  );
};

const onSession = (resp: Session) => {
  console.log(`Received session: `, resp);
  sessionXid = resp.sessionXid;
};

/*
  Connect the client and send messages
*/
(async function () {
  const [eventName, data] = getInputOrDefault();

  const client = new WebsocketClient(`ws://localhost:${config.port}`);

  await client.connect();

  // attach pong handler
  client.on(wsEventNames.PONG, onPong);
  client.on(wsEventNames.SESSION_CREATED, onSession);

  // send input or example data
  console.log(
    `sending '${eventName}' with data: ` +
      `${JSON.stringify(data, undefined, 2)}`
  );
  client.send(eventName, data);

  // send a ping
  console.log(`sending '${wsEventNames.PING}'...`);
  client.send(wsEventNames.PING);

  client.close();
})();
