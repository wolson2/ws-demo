import WebsocketServer from "../ws/WebsocketServer";
import { PingListener } from "./listeners/PingListener";
import { PingWebsocket } from "./websockets/PingWebsocket";

/*
  Start up a new websocket server and listen for incoming messages
*/
const ws = new WebsocketServer();

// add listeners
new PingListener(new PingWebsocket(ws));

// start the server
ws.listen();
