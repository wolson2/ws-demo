import WssListener from "../../ws/WssListener";
import { PingWebsocket } from "../websockets/PingWebsocket";

export class PingListener extends WssListener<PingWebsocket> {
  public attach() {
    this.service.onPing(this.handlePingMessage.bind(this));
  }

  private handlePingMessage() {
    console.log("ping message recieved in handler");
    this.service.sendPong({ timestamp: new Date() });
  }
}
