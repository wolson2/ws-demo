import WssService from "../../ws/WssService";
import { wsEventNames } from "../../ws/WebsocketTypes";

export class PingWebsocket extends WssService {
  public onPing(handler: any) {
    this.server.on(wsEventNames.PING, handler);
  }

  public sendPong(data: any) {
    this.server.send(wsEventNames.PONG, data);
  }
}
