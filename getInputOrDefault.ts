const args: string[] = process.argv as string[];

export default (): any[] => {
  const defaultMessage: string = '{"example":"data"}';
  let channel: string = "test";
  let data: any;

  if (args.length >= 3) {
    channel = args[2] || channel;
  }

  if (args.length >= 4) {
    try {
      data = JSON.parse(args[3] || defaultMessage);
    } catch (_) {
      data = args[3];
    }
  }

  return [channel, data];
};
